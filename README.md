## Task
1) Write a function that will be found in the text most often
word used.
2) Given the number of seconds that specifies the time interval. Write a function which
must count the number of minutes that have elapsed since the last
full hour.
3) Write a web application in which the following should be present
functionality:
1. Display all employees on the screen in the form of a table (you can
use bootstrap).
2. Form to add a new employee (preferably
use ajax).
3. Search for an employee on the base.

Additional task (perform if time and desire :)
1. Authorization
2. Ability to edit and delete employees
## How to deploy
- clone this repository and go to the project directory
- run command "composer install"
- rename '.env.example' to '.env' and set connection to created new database
- run next command:
- php artisan key:generate
- php artisan jwt:secret
- php artisan migrate
- php artisan db:seed
- run command "npm install" and 'npm run watch'
- Start server with command 'php artisan serve'. For default site open in  'http://127.0.0.1:8000' but if you have another address you need to change 'axios.defaults.baseURL' in 'resources/js/app.js' .



