
require('./bootstrap');
import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import App from './App.vue';
import Dashboard from './components/Dashboard.vue';
import FirstTask from './components/FirstTask.vue';
import Home from './components/Home.vue';
import Register from './components/Register.vue';
import Login from './components/Login.vue';
import SecondTask from './components/SecondTask.vue';
import pagination from 'laravel-vue-pagination';
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.component('pagination', require('laravel-vue-pagination'));
axios.defaults.baseURL = 'http://127.0.0.1:8000/api';
const router = new VueRouter({
    routes: [{
        path: '/',
        name: 'home',
        component: Home
    },{
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
            auth: false
        }
    },{
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            auth: false
        }
    },{
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard,
        meta: {
            auth: true
        }
    }, {
        path: '/first_task',
        name: 'firstTask',
        component: FirstTask,
        meta: {
            auth: true
        }
    },{
        path: '/second_task',
        name: 'secondTask',
        component: SecondTask,
        meta: {
            auth: true
        }
    }
    ],
    linkActiveClass: "current"
});
Vue.router = router;
Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
});
App.router = Vue.router
// new Vue(App).$mount('#app');
const app = new Vue({
    el: '#app',
    components: {
        App,
        pagination
    },
    render: h => h(App)
});