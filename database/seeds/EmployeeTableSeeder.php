<?php

use Illuminate\Database\Seeder;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        for($i = 0; $i <= 100; $i++){
            DB::table('employees')->insert([
                'fname'=> $faker->name,
                'sname'=> $faker->lastName,
                'pname'=> $faker->userName,
            ]);
        }
    }
}
